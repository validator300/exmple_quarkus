package com.example.repo;

import com.example.pojo.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class UserRepository implements PanacheRepository<User> {
}

package com.example.service;

import com.example.pojo.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class ServiceResource implements PanacheRepository<User> {

    public void getEmployeeById(Long id){
        User user = User.findById(id);
        if (user != null) {
            log.info("user : " + user);
            Response.ok(user).build();
        } else {
            log.info("user is null");
            Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    public Response createEmployee(User user){
        User.persist(user);
        log.info("create new employee: " + user.toString());
        return Response.status(Response.Status.CREATED).build();
    }

    public String deleteEmployeeForId(Long id){
        boolean delete = User.deleteById(id);
        if (delete) {
            return "user with id = " + id + " is deleted";
        } else {
            return "user with id = " + id + " is not deleted : " + delete;
        }
    }
}

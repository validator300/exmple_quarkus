package com.example.web;

import com.example.pojo.User;
import com.example.service.ServiceResource;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

@Path("/api")
@Slf4j
@Produces(MediaType.APPLICATION_JSON)
public class ControllerResource {
    @Inject
    ServiceResource serviceResource;

    @GET
    @Path("/employee/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getEmployeeById(@PathParam("id") Long id) {
        serviceResource.getEmployeeById(id);
    }

    @POST
    @Path("/employee/")
    @Transactional
    public Response createEmployee(User user) {
        return serviceResource.createEmployee(user);
    }

    @GET
    @Path("/employee")
    public Response getText() {
        return Response.ok("hello").build();
    }

    @DELETE
    @Path("/employee/{id}")
    public String deleteEmployeeForId(@PathParam("id") Long id) {
        return serviceResource.deleteEmployeeForId(id);
    }
}
